const { db } = require("../model/database.js");

const user = db.user;

exports.readAllUserData = (req, res) => {
  user
    .findAll()
    .then((data) => res.send(data).status(200))
    .catch((err) =>
      res
        .send({
          message: err,
        })
        .status(500)
    );
};

exports.updateOrCreateUser = async (req, res) => {
  const { userId, userName } = req.body;

  if (!userId) {
    const count = await user.count();

    return user.create({ ...req.body, userId: count + 1 }).then(() =>
      res
        .send({
          message: `Create User ${userName} successful`,
        })
        .status(200)
    );
  }

  user
    .update(req.body, { where: { userId: userId } })
    .then((num) => {
      if (num == 1) {
        res
          .send({
            message: "Updated successfully.",
          })
          .status(200);
      } else {
        res
          .send({
            message: `Cannot update User ${userName}.`,
          })
          .status(500);
      }
    })
    .catch((err) => {
      res.status(500);
    });
};

exports.deleteUser = (req, res) => {
  const { userId, userName } = req.body;

  user
    .destroy({ where: { userId: userId } })
    .then((num) => {
      if (num == 1) {
        res
          .send({
            message: `User ${userName} was deleted successfully!`,
          })
          .status(200);
      } else {
        res
          .send({
            message: `Cannot delete User ${userName}.`,
          })
          .status(500);
      }
    })
    .catch((err) => {
      res
        .send({
          message: "Could not delete User",
        })
        .status(500);
    });
};
