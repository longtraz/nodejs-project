module.exports = (sequelize, Sequelize) => {
  const user = sequelize.define("user", {
    userId: {
      type: Sequelize.INTEGER,
    },
    userName: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
    },
    age: {
      type: Sequelize.INTEGER,
    },
  });

  return user;
};
