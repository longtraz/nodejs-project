const express = require("express");
const bodyParser = require("body-parser");

const {
  readAllUserData,
  updateOrCreateUser,
  deleteUser,
} = require("./controller/user");

const { db } = require("./model/database");

const app = express();

const port = 8080;

app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }));

db.sequelize
  .sync()
  .then(async () => {
    await db.user.sync();
    console.log("Synced db.");
  })
  .catch((err) => {
    console.log("Failed to sync db: " + err.message);
  });

app.get("/user", readAllUserData);

app.post("/user", updateOrCreateUser);

app.delete("/user", deleteUser);

app.listen(port, () => {
  console.log(`Server's live now!`);
});
